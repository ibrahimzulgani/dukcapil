<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?= base_url ('assets/images/favicon.png')?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?= base_url ('assets/images/favicon.png')?>" type="image/x-icon">
    <title>Poco - Premium Admin Template</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/fontawesome.css')?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/icofont.css')?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/themify.css')?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/flag-icon.css')?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/feather-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/animate.css')?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/chartist.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/date-picker.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/prism.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/material-design-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/pe7-icon.css')?>">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/bootstrap.css')?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/style.css')?>">
    <link id="color" rel="stylesheet" href="<?= base_url ('assets/css/color-1.css')?>" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?= base_url ('assets/css/responsive.css')?>">
    
    <script src="<?= base_url('assets/js/chart/apex-chart/apex-chart.js')?>"></script>
  </head>
  <body>
      <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>New Era Admin Loading..</h1>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <div class="page-main-header">
        <?= $header ?>
      </div>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
       
        <div class="page-body" style="margin-left:0">
          <?= $content?>
        </div>

        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Disdukcapilsumedangkab © 2021. All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
              </div>
            </div>
          </div>
        </footer>
      </div>

      <!-- latest jquery-->
    <script src="<?= base_url('assets/js/jquery-3.5.1.min.js')?>"></script>
    <!-- Bootstrap js-->
    <script src="<?= base_url('assets/js/bootstrap/popper.min.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap/bootstrap.js')?>"></script>
    <!-- feather icon js-->
    <script src="<?= base_url('assets/js/icons/feather-icon/feather.min.js')?>"></script>
    <script src="<?= base_url('assets/js/icons/feather-icon/feather-icon.js')?>"></script>
    <!-- Sidebar jquery-->
    <script src="<?= base_url('assets/js/sidebar-menu.js')?>"></script>
    <script src="<?= base_url('assets/js/config.js')?>"></script>
    <!-- Plugins JS start-->
    <script src="<?= base_url('assets/js/typeahead/handlebars.js')?>"></script>
    <script src="<?= base_url('assets/js/typeahead/typeahead.bundle.js')?>"></script>
    <script src="<?= base_url('assets/js/typeahead/typeahead.custom.js')?>"></script>
    <script src="<?= base_url('assets/js/typeahead-search/handlebars.js')?>"></script>
    <script src="<?= base_url('assets/js/typeahead-search/typeahead-custom.js')?>"></script>
    <script src="<?= base_url('assets/js/chart/chartist/chartist.js')?>"></script>
    <script src="<?= base_url('assets/js/chart/chartist/chartist-plugin-tooltip.js')?>"></script>
    <script src="<?= base_url('assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?= base_url('assets/js/prism/prism.min.js')?>"></script>
    <script src="<?= base_url('assets/js/clipboard/clipboard.min.js')?>"></script>
    <script src="<?= base_url('assets/js/counter/jquery.waypoints.min.js')?>"></script>
    <script src="<?= base_url('assets/js/counter/jquery.counterup.min.js')?>"></script>
    <script src="<?= base_url('assets/js/counter/counter-custom.js')?>"></script>
    <script src="<?= base_url('assets/js/custom-card/custom-card.js')?>"></script>
    <script src="<?= base_url('assets/js/notify/bootstrap-notify.min.js')?>"></script>
    <script src="<?= base_url('assets/js/dashboard/default.js')?>"></script>
    <script src="<?= base_url('assets/js/notify/index.js')?>"></script>
    <script src="<?= base_url('assets/js/datepicker/date-picker/datepicker.js')?>"></script>
    <script src="<?= base_url('assets/js/datepicker/date-picker/datepicker.en.js')?>"></script>
    <script src="<?= base_url('assets/js/datepicker/date-picker/datepicker.custom.js')?>"></script>
    <script src="<?= base_url('assets/js/chat-menu.js')?>"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?= base_url('assets/js/script.js')?>"></script>
    <script src="<?= base_url('assets/js/theme-customizer/customizer.js')?>"></script>
    <!-- login js-->
    <!-- Plugin used-->
  </body>
</html>
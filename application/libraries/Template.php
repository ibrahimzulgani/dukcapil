<?php
class Template{
    protected $_ci;

    function __construct(){
        $this->_ci = &get_instance();
    }

    function frontend ($content, $data = NULL, $sidebar=true){
        $data['header'] = $this->_ci->load->view('frontend/template/header', $data, TRUE);
        
        if($sidebar){
            $data['sidebar'] = $this->_ci->load->view('frontend/template/sidebar', $data, TRUE);
        }

        $data['content'] = $this->_ci->load->view('frontend/'. $content, $data, TRUE);
        $this->_ci->load->view('frontend/template/index', $data);
    }
}
<?php

class Dashboard_Model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function getJmlPenduduk() {
        $sql = "select sum(jw_l) as cowo, sum(jw_p) as cewe from t_penduduk";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getPerkecamatan() {
        $sql = "SELECT kec, SUM(jw_l) as cowo, SUM(jw_p) as cewe FROM `t_penduduk` GROUP by kec";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getTotalPenduduk() {
        $sql = "select sum(j_jw) as total from t_penduduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getRataTembuh() {
        $sql = "select avg(pertumbuhan_biodata) as total from t_pertumbuhan";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getTotalLaki() {
        $sql = "select sum(jw_l) as total from t_penduduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getTotalPerempuan() {
        $sql = "select sum(jw_p) as total from t_penduduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getKtpKia() {
        $sql = "select * from t_ktpkia";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getStatusKawin() {
        $sql = "SELECT SUM(bk_J) as bk, SUM(k_J) as kawin, SUM(ch_J) as ch, SUM(cm_J) as cm FROM t_statuskawin";
        $query = $this->db->query($sql);
        return $query->result();
      }

    public function getPerAgama() {
        $sql = "select sum(J_islam) as islam, sum(J_kristen) as kristen, sum(J_khatolik) as khatolik, sum(J_hindu) as hindu, sum(J_budha) as budha, sum(J_konghucu) as konghucu, sum(J_kep) as kepercayaan from t_agama";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPerGoldarah(){
        $sql = "select sum(A) as A, sum(B) as B, sum(AB) as AB, sum(O) as O, sum(A_plus) as A_plus, sum(A_minus) as A_minus, sum(B_plus) as B_plus, sum(B_minus) as B_minus, sum(AB_plus) as AB_plus, sum(AB_minus) as AB_minus, sum(O_plus) as O_plus, sum(O_minus) as O_minus from t_goldarah";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getWajibKtp(){
        $sql = "select * from t_ktpkia";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPendidikan(){
        $sql = "select sum(tb_sekolahJML) as tb_sekolah, sum(bt_sdJML) as bt_sd, sum(tmt_sdJML) as tmt_sd, sum(sltpJML) as sltp, sum(sltaJML) as slta, sum(d1JML) as d1, sum(d3JML) as d3, sum(s1JML) as s1, sum(s2JML) as s2, sum(s3JML) as s3 from t_pendidikan";
        $query = $this->db->query($sql);
        return $query->result();

    }

}
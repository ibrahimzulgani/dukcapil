<?php


class MY_Controller extends CI_Controller
{
    public function render_backend($content, $data=Null)
    {
        $data['header'] = $this->load->view('template/backend/header', $data, true);
        $data['content'] = $this->load->view($content, $data, true);
        $data['footer'] = $this->load->view('template/backend/footer', $data, true);

        $this->load->view('template/backend/index', $data);
    }
}

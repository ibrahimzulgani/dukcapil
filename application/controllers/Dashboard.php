<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
        parent::__construct();

		$this->load->model("Dashboard_Model");
    }

	public function index() {
		$data = [];

		$data['total_penduduk'] = $this->Dashboard_Model->getTotalPenduduk();
		$data['rata_tembuh'] = $this->Dashboard_Model->getRataTembuh();
		$data['total_laki'] = $this->Dashboard_Model->getTotalLaki();
		$data['total_perempuan'] = $this->Dashboard_Model->getTotalPerempuan();
		$data['jml_penduduk'] = $this->Dashboard_Model->getJmlPenduduk();
		$data['status_kawin'] = $this->Dashboard_Model->getStatusKawin();
		
		$dataPerkecamatan = $this->Dashboard_Model->getPerkecamatan();
		// $dataKec = "";
		$dataCowo = "";
		$dataCewe = "";
		for ($i=0; $i < count($dataPerkecamatan); $i++) { 
			if($i==0){
				// $dataKec = $dataPerkecamatan[$i]->kec;
				$dataCowo = intval($dataPerkecamatan[$i]->cowo);
				$dataCewe = intval($dataPerkecamatan[$i]->cewe);
			} else {
				// $dataKec .= ",".$dataPerkecamatan[$i]->kec;
				$dataCowo .= ",".intval($dataPerkecamatan[$i]->cowo);
				$dataCewe .= ",".intval($dataPerkecamatan[$i]->cewe);
			}
			
		}
		// $data["perKecamatanKec"] = $dataKec;
		$data["perKecamatanCowo"] = $dataCowo;
		$data["perKecamatanCewe"] = $dataCewe;

		$dataKtpKia = $this->Dashboard_Model->getKtpKia();
		$dataKec = "";
		$dataKtpL = "";
		$dataKtpP = "";
		$dataKiaL = "";
		$dataKiaP = "";
		for ($i=0; $i < count($dataKtpKia); $i++) { 
			if($i==0){
				$dataKec = $dataKtpKia[$i]->kec;
				$dataKtpL = intval($dataKtpKia[$i]->KTPL);
				$dataKtpP = intval($dataKtpKia[$i]->KTPP);
				$dataKiaL = intval($dataKtpKia[$i]->KIAL);
				$dataKiaP = intval($dataKtpKia[$i]->KIAP);
			} else {
				$dataKec .= ",".$dataKtpKia[$i]->kec;
				$dataKtpL .= ",".intval($dataKtpKia[$i]->KTPL);
				$dataKtpP .= ",".intval($dataKtpKia[$i]->KTPL);
				$dataKiaL .= ",".intval($dataKtpKia[$i]->KIAL);
				$dataKiaP .= ",".intval($dataKtpKia[$i]->KIAP);
			}
			
		}
		$data["data_kec"] = $dataKec;
		$data["data_ktp_l"] = $dataKtpL;
		$data["data_ktp_p"] = $dataKtpP;
		$data["data_kia_l"] = $dataKiaL;
		$data["data_kia_p"] = $dataKiaP;
		
		$data["jmlPenduduk"] = $this->Dashboard_Model->getJmlPenduduk();
		// $data["perKecamatan"] = $this->Dashboard_Model->getPerKecamatan();
		$data["perAgama"] = $this->Dashboard_Model->getPerAgama();
		$data["perGoldarah"] = $this->Dashboard_Model->getPerGoldarah();
		$data["pendidikan"] = $this->Dashboard_Model->getPendidikan();
		$this->template->frontend('dashboard/index', $data);
	}
	
}
